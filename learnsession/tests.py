from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


class LearnjsUnitTest(TestCase):
	@classmethod
	def setUpClass(cls):
		super().setUpClass()
		cls.user = User.objects.create_user('valnicolas', 'nicolas@gmail.com', 'valentinus')
		cls.user.first_name = 'valentinus'
		cls.user.save()

	def test_status_url_exists(self):
		response = Client().get('//')
		self.assertEqual(response.status_code, 200)
		
	def test_template_used(self):
		response = Client().get('//')
		self.assertTemplateUsed(response, 'index.html')

	def test_file_function(self):
		response = Client().get("/sukses")
		self.assertEqual(response.status_code, 301)

	def test_logout_url_exists(self):
		response = Client().get('/logout')
		self.assertEqual(response.status_code, 301)

	def test_index_function(self):
		found = resolve('/')
		self.assertEqual(found.func, views.index)

	def test_username_valid(self):
		form_data = {
			'username': 'valnicolas',
			'password': 'valentinus'
		}
		user = authenticate(data=form_data)
		self.assertTrue(user is None);

	def test_LogIn_submit(self):
		response = self.client.post(
			reverse('learnsession:my_view'), data={
				'username': 'valnicolas',
				'password' : 'valentinus',
			}
		)
		self.assertEqual(response.status_code, 302)
		response = self.client.get(reverse('learnsession:sukseslogin'))
		html = response.content.decode()
		self.assertIn(self.user.username, html)

	def test_LogIn_not_Valid(self):
		response = self.client.post(
			reverse('learnsession:my_view'), data={
				'username': 'feoifs',
				'password' : 'valdawdentinus',
			}
		)
		self.assertEqual(response.status_code, 200)

	def test_SignIn_SignOut(self):
		response = self.client.post(
			reverse('learnsession:my_view'), data={
				'username': 'valnicolas',
				'password' : 'valentinus',
				}
			)
		self.client.get(reverse('learnsession:sukseslogout'))
		response = self.client.get(reverse('learnsession:index'))
		html = response.content.decode()
		self.assertNotIn(self.user.username, html)


# Create your tests here.
