from django.urls import path
from . import views

app_name = 'learnsession'

urlpatterns = [
    path('', views.index, name='index'),
    path("login/", views.my_view, name='my_view'),
    path("sukses/", views.sukseslogin, name='sukseslogin'),
    path("logout/", views.sukseslogout, name='sukseslogout')
    # dilanjutkan ...
]
