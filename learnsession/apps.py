from django.apps import AppConfig


class LearnsessionConfig(AppConfig):
    name = 'learnsession'
