from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

def index(request):
	return render(request,'index.html')

def my_view(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(request, username=username, password=password)
	if user is not None:
		login(request, user)
		request.session['my_session'] = username
		return redirect("learnsession:sukseslogin")
	else:
		gagal = "Password/Username salah!"
		return render(request,'index.html', {"gagal":gagal})

def sukseslogin(request):
	if request.session.get('my_session') is not None:
		value = request.session['my_session']
	return render(request, 'login.html', {"nama": value})

def sukseslogout(request):
	logout(request)
	return redirect("learnsession:index")

  

# Create your views here.
